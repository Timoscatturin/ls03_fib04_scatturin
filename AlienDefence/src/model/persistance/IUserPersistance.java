package model.persistance;

import model.persistanceDummy.User;

public interface IUserPersistance {

	User readUser(String username);

}